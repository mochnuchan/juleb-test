---NOTES---

Modul bernama "inventory_translation" untuk mentranslate secara otomatis field name pada menu "Operation Types" ketika create atau edit.
Modul tersebut memerlukan library "translate" untuk keperluan menerjemahkan kata.

Di dalam database yang dicantumkan, terdapat 3 user deengan language yang berbeda:
1. Administrator (user:admin/pass:admin) -> English
2. Nuchan (user:admin2/pass:123) -> Indonesia
3. Aril (user:admin3/pass:123) -> Arabic

Langkah-langkah untuk mencoba translation Operation Types:
- Buka menu Inventory -> Configuration -> Warehouse Management -> Operation Types
- Klik button create untuk membuat record baru
- Untuk melihat hasil terjemahan field name pada menu Operation Types, bisa dilakukan dengan login menggunakan user yang lain dengan language yang berbeda