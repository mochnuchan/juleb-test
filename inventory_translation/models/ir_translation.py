from odoo import api, fields, models, tools, SUPERUSER_ID, _
from translate import Translator


class IrTranslationCustom(models.Model):
    _inherit = "ir.translation"

    @api.model
    def _set_ids(self, name, tt, lang, ids, value, src=None):
        self._modified_model(name.split(',')[0])
        search_lang = self.env['res.lang'].search([('code', '=', lang)], limit=1)
        translator = Translator(to_lang=search_lang.iso_code)
        translated_field = translator.translate(value)

        # update existing translations
        self._cr.execute("""UPDATE ir_translation
                                SET value=%s, src=%s, state=%s
                                WHERE lang=%s AND type=%s AND name=%s AND res_id IN %s
                                RETURNING res_id""",
                         (translated_field, src, 'translated', lang, tt, name, tuple(ids)))
        existing_ids = [row[0] for row in self._cr.fetchall()]

        # create missing translations
        self.sudo().create([{
            'lang': lang,
            'type': tt,
            'name': name,
            'res_id': res_id,
            'value': value,
            'src': src,
            'state': 'translated',
        }
            for res_id in set(ids) - set(existing_ids)
        ])
        return len(ids)