from odoo import api, fields, models
from translate import Translator


class StockPickingTypeCustom(models.Model):
    _inherit = 'stock.picking.type'

    @api.model
    def create(self, vals):
        res = super(StockPickingTypeCustom, self).create(vals)
        self.generate_translation('create', vals, res)
        return res

    def write(self, vals):
        self.generate_translation('write', vals, self)
        res = super(StockPickingTypeCustom, self).write(vals)
        return res

    def unlink(self):
        self.generate_translation('delete', [], self)
        res = super(StockPickingTypeCustom, self).unlink()
        return res

    def generate_translation(self, operation, vals, res):
        langs = self.env['res.lang'].sudo().get_installed()
        for lang in langs:
            if operation == 'create':
                fields = self.env['ir.model.fields'].sudo().search([('model', '=', self._name), ('ttype', '=', 'char'), ('translate', '=', True)])
                for field in fields:
                    if vals.get(field.name):
                        self.env['ir.translation'].create({
                            'src': vals.get(field.name),
                            'value': vals.get(field.name),
                            'name': self._name + ',' + field.name,
                            'lang': lang[0],
                            'type': 'model',
                            'res_id': res.id,
                            'module': self._module,
                        })
                        search_translation = self.env['ir.translation'].search([
                            ('src', '=', vals.get(field.name)),
                            ('name', '=', self._name + ',' + field.name),
                            ('type', '=', 'model'),
                            ('res_id', '=', res.id),
                        ])
                        list_lang = []
                        for trans in search_translation:
                            search_lang = self.env['res.lang'].search([('code', '=', trans.lang)], limit=1)
                            translator = Translator(to_lang=search_lang.iso_code)
                            translated_field = translator.translate(vals.get(field.name))
                            trans.write({
                                'value': translated_field,
                                'module': self._module,
                            })
                            list_lang.append(trans.lang)
                        for lang2 in langs:
                            if lang2[0] not in list_lang:
                                search_lang = self.env['res.lang'].search([('code', '=', lang2[0])], limit=1)
                                translator = Translator(to_lang=search_lang.iso_code)
                                translated_field = translator.translate(vals.get(field.name))
                                self.env['ir.translation'].create({
                                    'src': vals.get(field.name),
                                    'value': translated_field,
                                    'name': self._name + ',' + field.name,
                                    'lang': lang2[0],
                                    'type': 'model',
                                    'res_id': res.id,
                                    'module': self._module,
                                })
                break
            elif operation == 'write':
                fields = self.env['ir.model.fields'].sudo().search([('model', '=', self._name), ('ttype', '=', 'char'), ('translate', '=', True)])
                for field in fields:
                    if vals.get(field.name):
                        search_translation = self.env['ir.translation'].search([
                            ('name', '=', self._name + ',' + field.name),
                            ('type', '=', 'model'),
                            ('res_id', '=', res.id),
                        ])
                        for trans in search_translation:
                            search_lang = self.env['res.lang'].search([('code', '=', trans.lang)], limit=1)
                            translator = Translator(to_lang=search_lang.iso_code)
                            translated_field = translator.translate(vals.get(field.name))
                            trans.write({
                                'src': vals.get(field.name),
                                'value': translated_field,
                                'module': self._module,
                            })
                break
            else:
                for result in res:
                    fields = self.env['ir.model.fields'].sudo().search([('model', '=', self._name), ('ttype', '=', 'char'), ('translate', '=', True)])
                    for field in fields:
                        search_translation = self.env['ir.translation'].search([
                            ('name', '=', self._name + ',' + field.name),
                            ('type', '=', 'model'),
                            ('res_id', '=', result.id),
                        ])
                        for trans in search_translation:
                            trans.unlink()
                break