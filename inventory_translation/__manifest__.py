
{
    'name': 'Inventory Translation',
    'version': '1.1',
    'summary': 'Inventory Translation',
    'description': "",
    'website': 'https://www.odoo.com/page/warehouse',
    'depends': ['base', 'stock'],
    'category': 'Operations/Inventory',
    'sequence': 13,
    'demo': [
    ],
    'data': [
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
